***Settings***
Library     SeleniumLibrary
Library     libs/db.py

Resource    kws.robot
Resource    hooks.robot
Resource    services.robot

Resource    pageObject/LoginPO.robot
Resource    pageObject/CustomersPO.robot
Resource    pageObject/EquipoPO.robot
Resource    pageObject/ContractsPO.robot

Resource    components/Sidebar.robot
Resource    components/Toaster.robot

***Variables***
${base_url}         http://zepalheta-web:3000/
${admin_user}       admin@zepalheta.com.br
${admin_password}   pwd123