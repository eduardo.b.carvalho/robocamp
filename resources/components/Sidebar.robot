***Settings***
Documentation   Representação do menu lateral da área logada

***Variables***
${NAV_CUSTOMERS}    css:a[href$=customers]
${NAV_CONTRACTS}    css:a[href$=contracts]
${NAV_EQUIPOS}      css:a[href$=equipos]

***Keywords***
Go to contracts
    Wait until element is visible   ${NAV_CONTRACTS}
    Click element                   ${NAV_CONTRACTS}