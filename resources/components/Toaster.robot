***Settings***
Documentation   Representação do toaster

***Variables***
${TOASTER_SUCCESS}    css:div[type=success] strong
${TOASTER_ERROR_P}    css:div[type=error] p
${TOASTER_ERROR}      css:div[type=error] strong
${TOASTER_CLOSE}      xpath://*[@id="root"]/div[2]/div/button
