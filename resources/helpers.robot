***Settings***

Library               OperatingSystem

***Keywords***
Get json
    [Arguments]          ${file}
    ${json_file}         Get File        ${EXECDIR}/resources/fixtures/${file}
    ${jason_dict}=       Evaluate        json.loads($json_file)   json

    [Return]        ${jason_dict}
