***Keywords***
Start session
    Run Keyword If      "${browser}"=="headless"
    ...     Open chrome headless

    Run Keyword If      "${browser}"=="chrome"
    ...     Open chrome

    Set window size     1440    900

Finish TestCase
    Capture Page Screenshot

Finish session
    Close Browser

Login session
    Start session
    Go to           ${base_url}
    Login with      ${admin_user}      ${admin_password}

### Webdriver (para funcionar no jenkins)
Open chrome headless
    Open Browser    about:blank     headlesschrome     options=add_argument('--disable-dev-shm-usage')

Open chrome
    Open Browser    about:blank     chrome      options=add_experimental_option('excludeSwitches',['enable-logging'])