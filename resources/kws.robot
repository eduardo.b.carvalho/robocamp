***Keywords***
##Login
Acesso a pagina de login
    Go to    ${base_url}

Submeto minhas credenciais
    [Arguments]     ${email}    ${password}
    Login with      ${email}    ${password}

Devo ver a área logada
    Wait until page contains   Aluguéis     5

Devo ver um alerta com a mensagem
    [Arguments]                                         ${expected_message}
    wait until Element contains     ${TOASTER_ERROR}    ${expected_message}
    Wait until Element is visible   ${TOASTER_CLOSE}
    Click Element                   ${TOASTER_CLOSE}

Devo ver um alerta com a mensagem de erro
    [Arguments]                                         ${expected_message}
    wait until Element contains     ${TOASTER_ERROR_P}  ${expected_message}
    Wait until Element is visible   ${TOASTER_CLOSE}
    Click Element                   ${TOASTER_CLOSE}

##Cadastro
Dado que acesso o formulário de cadastro de cliente
    Wait until Element is visible   ${NAV_CUSTOMERS}
    Click Element   ${NAV_CUSTOMERS}
    Click Element   ${CUSTOMERS_FORM}

E que eu tenho o seguinte cliente:
    [Arguments]             ${nome}     ${cpf}  ${endereco}     ${telefone}
    Remove customer by cpf  ${cpf}

    Set Test Variable  ${nome}
    Set Test Variable  ${cpf}
    Set Test Variable  ${endereco}
    Set Test Variable  ${telefone}

Quando faço a inclusão desse cliente
    Registrar novo cliente  ${nome}     ${cpf}  ${endereco}     ${telefone}

Mas este CPF já existe no sistema
    insert customer    ${nome}     ${cpf}  ${endereco}     ${telefone}

Então devo ver a notificação:
    [Arguments]                                                 ${msg_cadastro_esperado}
    Wait until Element contains     ${TOASTER_SUCCESS}          ${msg_cadastro_esperado}

Então devo uma mensagem informando que os campos do cadastro de clientes são obrigatórios
    Wait until Element contains     ${LABEL_NAME}           Nome é obrigatório
    Wait until Element contains     ${LABEL_CPF}            CPF é obrigatório
    Wait until Element contains     ${LABEL_ADDRESS}        Endereço é obrigatório
    Wait until Element contains     ${LABEL_PHONE}          Telefone é obrigatório

Então devo ver o texto:
    [Arguments]                     ${msg_saida}
    Wait until page contains        ${msg_saida}

E esse cliente deve ser exibido na lista
    ${cpf_formatado}=       format cpf      ${cpf}
    #Click element           xpath://button[text()='VOLTAR']
    Go back
    Wait until element is visible    ${CUSTOMERS_LIST}
    Table should contain             ${CUSTOMERS_LIST}   ${cpf_formatado}

##Remover cadastro
Dado que eu tenho um cliente indesejado:
    [Arguments]             ${nome}     ${cpf}  ${endereco}     ${telefone}
    Remove customer by cpf  ${cpf}
    Insert customer         ${nome}     ${cpf}  ${endereco}     ${telefone}

    Set Test Variable       ${cpf}

E acesso a lista de clientes
    Wait until Element is visible   ${NAV_CUSTOMERS}
    Click Element                   ${NAV_CUSTOMERS}

Quando eu removo esse cliente
    ${cpf_formatado}=   format cpf  ${cpf}
    Set Test Variable       ${cpf_formatado}

    Go to customer details  ${cpf_formatado}
    Click remove customer

E esse cliente não deve aparecer na lista
    Wait until page does not contain   ${cpf_formatado}

##Equipamento
Dado que acesso o formulário de equipamentos
    Wait until Element is visible   ${NAV_EQUIPOS}
    Click Element                   ${NAV_EQUIPOS}
    Click Element                   ${EQUIPO_FORM}

E que eu tenho o seguinte equipamento:
    [Arguments]             ${equipo}     ${diaria}
    Remove equip  ${equipo}

    Set Test Variable  ${equipo}
    Set Test Variable  ${diaria}

Quando faço a inclusão desse equipamento
    Registrar novo equipamento  ${equipo}     ${diaria}

Mas este equipamento já existe no sistema
    insert equip    ${equipo}     ${diaria}

## Contratos
Dado que eu tenha o seguinte cliente cadastrado:
    [Arguments]         ${file_name}
    ${customer}=        Get json    customers/${file_name}
    Delete customer     ${customer['cpf']}
    Post Customer       ${customer}

    Set test Variable   ${customer}

E este cliente deseja alugar o seguinte equipo:
    [Arguments]         ${file_name}
    ${equipo}=          Get json    equipos/${file_name}
    Post Equipo         ${equipo}

    Set test Variable   ${equipo}

E acesso o formulário de contratos
    Go to contracts
    Click element       ${CONTRACTS_FORM}

Quando faço um novo contrato de locação
    Create new contract  ${customer['name']}  ${equipo['name']}

