***Settings***
Documentation   Representação da página de contratos com seus elementos e ações

***Variables***
${CONTRACTS_FORM}   css:a[href$=register]
${LABEL_NAME}       css:label[for=name]
${LABEL_CPF}        css:label[for=cpf]
${LABEL_ADDRESS}    css:label[for=address]
${LABEL_PHONE}      css:label[for=phone_number]
${CUSTOMERS_LIST}   css:table

***Keywords***
Create new contract
    [Arguments]     ${customer_name}    ${equipo_name}
    Click element       xpath://div[contains(text(),'Escolha o locatário')]
    Click element       xpath://div[contains(text(),'${customer_name}')]

    Click element       xpath://div[contains(text(),'Escolha o item')]
    Click element       xpath://div[contains(text(),'${equipo_name}')]

    Input text          id:delivery_price       10
    Input text          id:quantity             1

    Click element       css:button[type=submit]
    Click element       xpath://button[text()='CADASTRAR']