***Settings***
Documentation   Representação da página de clientes com seus elementos e ações

***Variables***
${CUSTOMERS_FORM}   css:a[href$=register]
${LABEL_NAME}       css:label[for=name]
${LABEL_CPF}        css:label[for=cpf]
${LABEL_ADDRESS}    css:label[for=address]
${LABEL_PHONE}      css:label[for=phone_number]
${CUSTOMERS_LIST}   css:table

***Keywords***
Registrar novo cliente
    [Arguments]     ${nome}     ${cpf}  ${endereco}     ${telefone}

    Input text      id:name             ${nome}
    Input text      id:cpf              ${cpf}
    Input text      id:address          ${endereco}
    Input text      id:phone_number     ${telefone}

    Click Element       xpath://button[text()='CADASTRAR']

Go to customer details
    [Arguments]     ${cpf_formatado}
    ${element}=     set Variable    xpath://td[text()='${cpf_formatado}']
    Wait until element is visible   ${element}
    Click Element                   ${element}

Click remove customer
    ${element}=     set Variable    xpath://button[text()="APAGAR"]
    Wait until element is visible   ${element}
    Click Element                   ${element}