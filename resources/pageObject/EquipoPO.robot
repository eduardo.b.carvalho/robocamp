***Settings***
Documentation   Representação da página de equipamento com seus elementos e ações

***Variables***
${EQUIPO_FORM}      css:a[href$="equipos/register"]
${LABEL_EQUIPO}     css:label[for=equipo-name]
${LABEL_DIARIA}     css:label[for=daily_price]


***Keywords***
Registrar novo equipamento
    [Arguments]     ${equipo}     ${diaria}

    Input text      id:equipo-name             ${equipo}
    Input text      id:daily_price             ${diaria}

    Wait until Element is not visible   ${TOASTER_CLOSE}    10
    Click Element       xpath://button[text()='CADASTRAR']



    

