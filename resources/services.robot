***Settings***
Documentation   Camada de serviços da minha aplicação

Library         RequestsLibrary
Resource        helpers.robot

***Variables***
${base_api_url}=    http://zepalheta-api:3333

***Keywords***
# POST /Sessions
Post Session
    [Arguments]     ${email}    ${senha}
    Create Session    zp-api             ${base_api_url}
    &{payload}=       Create Dictionary  email=${email}   password=${senha}
    &{headers}=       Create Dictionary  Content-Type=application/json
    ${resp}=          POST Request       zp-api             /sessions   data=${payload}   headers=${headers}
    [return]          ${resp}

# POST /customers
Post Customer
    [Arguments]     ${payload}
    Create Session    zp-api             ${base_api_url}
    ${token}=       Get session token
    &{headers}      Create Dictionary   Content-Type=application/json   Authorization=${token}
    ${resp}=        Post Request    zp-api  /customers  data=${payload}     headers=${headers}
    [Return]        ${resp}

# POST /equipos
Post Equipo
    [Arguments]     ${payload}
    Create Session    zp-api             ${base_api_url}
    ${token}=       Get session token
    &{headers}      Create Dictionary   Content-Type=application/json   Authorization=${token}
    ${resp}=        Post Request    zp-api  /equipos  data=${payload}     headers=${headers}
    [Return]        ${resp}

# PUT /customers
Put Customer
    [Arguments]     ${payload}      ${user_id}
    Create Session    zp-api             ${base_api_url}
    ${token}=       Get session token
    &{headers}      Create Dictionary   Content-Type=application/json   Authorization=${token}
    ${resp}=        Put Request    zp-api  /customers/${user_id}  data=${payload}     headers=${headers}
    [Return]        ${resp}

# DELETE /customers
Delete customer
    [Arguments]     ${cpf}
    ${token}=       Get session token
    &{headers}      Create Dictionary   Content-Type=application/json   Authorization=${token}

    ${resp}=        Delete Request  zp-api  /customers/${cpf}    headers=${headers}
    [Return]        ${resp}

Get session token
    ${resp}=    Post Session    admin@zepalheta.com.br  pwd123
    ${token}=   Convert to String   Bearer ${resp.json()['token']}
    [Return]    ${token}

# GET /customers
Get customers
    Create session      zp-api      ${base_api_url}
    ${token}=           Get session token
    &{headers}          Create Dictionary   Content-Type=application/json   Authorization=${token}

    ${resp}=            Get Request         zp-api      /customers      headers=${headers}
    [Return]            ${resp}

Get unique customer
    [Arguments]         ${user_id}
    Create session      zp-api      ${base_api_url}
    ${token}=           Get session token
    &{headers}          Create Dictionary   Content-Type=application/json   Authorization=${token}

    ${resp}=            Get Request         zp-api      /customers/${user_id}      headers=${headers}
    [Return]            ${resp}