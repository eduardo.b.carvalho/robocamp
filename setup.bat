docker-compose down
docker-compose up -d
docker exec -it zepalheta-api sh /home/node/api/setup.sh

echo '#######################################################################'
echo 'Os containers do Ze palheta já devem estar no ar'
docker ps

echo '#######################################################################'
echo 'Faça o hosteamento para o IP 127.0.0.1'

echo 'Hosts:'
echo 'zepalheta-postgres'
echo 'zepalheta-pgadmin'
echo 'zepalheta-api'
echo 'zepalheta-web'