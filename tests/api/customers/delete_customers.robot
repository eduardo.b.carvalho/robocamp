*** Settings ***
Library               Collections
Library               OperatingSystem
Resource              ../../../resources/services.robot

***Test Cases***
Delete customer
    ${origin}=           Get json                customers/chimbinha.json
    Delete customer      ${origin['cpf']}
    ${resp}=             Post Customer           ${origin}
    
    ${resp}=             Delete customer         ${origin['cpf']}

    Status should be    204                      ${resp}
    

Customer not found
    ${resp}=            Delete customer                 141.142.173-63
    Status should be    404                             ${resp}
    Should be equal     ${resp.json()['message']}       Customer not found
