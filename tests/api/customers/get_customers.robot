*** Settings ***
Library               Collections
Library               OperatingSystem
Resource              ../../../resources/services.robot

***Test Cases***
Get customer list

    ${list}=            Get json            customers/list.json

    FOR     ${item}     IN  @{list['data']}
            Post Customer   ${item}
    END

    ${resp}             Get customers
    Status should be    200     ${resp}
    ${total}=           Get Length      ${resp.json()}
    Should be true      ${total} > 0

Get unique customer
    ${origin}=           Get json                customers/unique.json
    Delete customer      ${origin['cpf']}
    ${resp}=             Post Customer           ${origin}
    ${user_id}=          Convert to string       ${resp.json()['id']}
    ${resp}=             Get unique customer     ${user_id}

    Status should be    200                      ${resp}
    Should be equal     ${resp.json()['cpf']}    ${origin['cpf']}

Customer not found
    ${resp}=            Get unique customer             698dc19d489c4e4db73e28a713eab07b
    Status should be    404                             ${resp}
    Should be equal     ${resp.json()['message']}       Customer not found
