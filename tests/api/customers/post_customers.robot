*** Settings ***
Library               Collections
Library               OperatingSystem
Resource              ../../../resources/services.robot

***Test Cases***
Novo cliente
    ${payload}=     Get json        customers/flea.json

    Delete customer     ${payload['cpf']}

    ${resp}=        Post Customer   ${payload}

    Status should be    200     ${resp}

Nome é obrigatório
    ${payload}=     Get json        customers/no_name.json
    
    ${resp}=        Post Customer   ${payload}

    Status should be    400     ${resp}
    Should be equal     ${resp.json()['message']}   "name" is required

CPF é obrigatório
    ${payload}=     Get json        customers/no_cpf.json

    ${resp}=        Post Customer   ${payload}

    Status should be    400     ${resp}
    Should be equal     ${resp.json()['message']}   "cpf" is required

Endereço é obrigatório
    ${payload}=     Get json        customers/no_address.json

    ${resp}=        Post Customer   ${payload}

    Status should be    400     ${resp}
    Should be equal     ${resp.json()['message']}   "address" is required

Telefone é obrigatório
    ${payload}=     Get json        customers/no_phone.json

    ${resp}=        Post Customer   ${payload}

    Status should be    400     ${resp}
    Should be equal     ${resp.json()['message']}   "phone_number" is required


