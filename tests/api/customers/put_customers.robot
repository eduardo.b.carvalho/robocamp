*** Settings ***
Library               Collections
Library               OperatingSystem
Resource              ../../../resources/services.robot

***Test Cases***
Update customer
    ${payload}=         Get json             customers/slash.json
    Delete customer     ${payload['cpf']}
    ${resp}=            Post Customer        ${payload}
    ${user_id}=         Convert to string    ${resp.json()['id']}

    Set to dictionary   ${payload}  name    Axl Rose

    ${resp}=        Put Customer    ${payload}  ${user_id}

    Status should be    204         ${resp}
    ${resp}=        Get unique customer         ${user_id}

    Should be equal     ${resp.json()['name']}     Axl Rose