*** Settings ***
Library               Collections
Resource              ../../../resources/services.robot

*** Test Cases ***
Login com sucesso
    ${resp}=            POST session      admin@zepalheta.com.br      pwd123
    Status should be    200     ${resp}

Senha incorreta
    ${resp}=            POST session      admin@zepalheta.com.br      abc123
    Status should be    401     ${resp}

Usuário não existe
    ${resp}=            POST session      admin@zebaqueta.com.br      pwd123
    Status should be    401     ${resp}
