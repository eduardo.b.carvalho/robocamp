***Settings***
Documentation   Cadastro de cliente
Resource    ../../resources/base.robot

Suite Setup      Login session
Suite Teardown   Finish session
Test Teardown    Finish TestCase

***Test Cases***
Novos clientes
    [Tags]  Smoke
    Dado que acesso o formulário de cadastro de cliente
    E que eu tenho o seguinte cliente:
    ...     Bon Jovi    40262159384     Rua dos bugs, 1000  219999911111
    Quando faço a inclusão desse cliente
    Então devo ver a notificação:   Cliente cadastrado com sucesso!
    E esse cliente deve ser exibido na lista

Cliente duplicado
    Dado que acesso o formulário de cadastro de cliente
    E que eu tenho o seguinte cliente:
    ...     Adrian Smith    56089613150     Rua dos bugs, 1000  219999911111
    Mas este CPF já existe no sistema
    Quando faço a inclusão desse cliente
    Devo ver um alerta com a mensagem   Este CPF já existe no sistema!

Campos obrigatorios
    Dado que acesso o formulário de cadastro de cliente
    E que eu tenho o seguinte cliente:
    ...     ${EMPTY}    ${EMPTY}    ${EMPTY}    ${EMPTY}
    Quando faço a inclusão desse cliente
    Então devo uma mensagem informando que os campos do cadastro de clientes são obrigatórios

Nome é obrigatório
    [Tags]      required
    [Template]  Validação de campos
    ${EMPTY}    84856012541     Rua Qualquer, 222   219999911111    Nome é obrigatório

CPF é obrigatório
    [Tags]      required
    [Template]  Validação de campos
    Nome teste    ${EMPTY}     Rua Qualquer, 222    219999911111        CPF é obrigatório

Endereço é obrigatório
    [Tags]      required
    [Template]  Validação de campos
    Nome teste    84856012541     ${EMPTY}          219999911111        Endereço é obrigatório

Telefone é obrigatório
    [Tags]      required
    [Template]  Validação de campos
    Nome teste    84856012541     Rua Qualquer, 222   ${EMPTY}          Telefone é obrigatório

Telefone incorreto
    [Tags]      required
    [Template]  Validação de campos
    Nome teste    84856012541     Rua Qualquer, 222   2199999111       Telefone inválido

***Keywords***
Validação de campos
    [Arguments]     ${nome}     ${cpf}      ${endereco}     ${telefone}     ${msg_saida}
    Dado que acesso o formulário de cadastro de cliente
    E que eu tenho o seguinte cliente:
    ...             ${nome}     ${cpf}      ${endereco}     ${telefone}
    Quando faço a inclusão desse cliente
    Então devo ver o texto:     ${msg_saida}