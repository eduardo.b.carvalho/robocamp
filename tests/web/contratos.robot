***Settings***
Documentation   Contratos de locação
Resource    ../../resources/base.robot

Suite Setup      Login session
Suite Teardown   Finish session
Test Teardown    Finish TestCase

***Test Cases***
Novo contrato de locação
    Dado que eu tenha o seguinte cliente cadastrado:    ozzy.json
    E este cliente deseja alugar o seguinte equipo:     meteoro.json
    E acesso o formulário de contratos
    Quando faço um novo contrato de locação
    Então devo ver a notificação:     Contrato criado com sucesso!