***Settings***
Documentation   Cadastro de equipamentos
Resource    ../../resources/base.robot

Suite Setup      Login session
Suite Teardown   Finish session
Test Teardown    Finish TestCase

***Test Cases***
Novo equipamento
    Dado que acesso o formulário de equipamentos
    E que eu tenho o seguinte equipamento:
    ...     Gibson SG   100.00
    Quando faço a inclusão desse equipamento
    Então devo ver a notificação:   Equipo cadastrado com sucesso!

Equipamento duplicado
    Dado que acesso o formulário de equipamentos
    E que eu tenho o seguinte equipamento:
    ...     Fender Telecaster   50.00
    Mas este equipamento já existe no sistema
    Quando faço a inclusão desse equipamento
    Devo ver um alerta com a mensagem   Erro na criação de um equipo

Nome obrigatório
    [Tags]              required
    [Template]          Validação de campos
    ${EMPTY}            80.00           Nome do equipo é obrigatório
Valor obrigatório
    [Tags]              required
    [Template]          Validação de campos
    Gibson Les Paul     ${EMPTY}        Diária do equipo é obrigatória


***Keywords***
Validação de campos
    [Arguments]     ${equipo}     ${diaria}     ${msg_saida}
    Dado que acesso o formulário de equipamentos
    E que eu tenho o seguinte equipamento:
    ...             ${equipo}     ${diaria}
    Quando faço a inclusão desse equipamento
    Então devo ver o texto:     ${msg_saida}