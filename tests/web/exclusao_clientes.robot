***Settings***
Documentation   Exclusão de cliente
Resource    ../../resources/base.robot

Suite Setup      Login session
Suite Teardown   Finish session
Test Teardown    Finish TestCase

***Test Cases***
Exclusao de cliente
    Dado que eu tenho um cliente indesejado:
    ...     Bob Dylan    11342638646     Rua dos bugs, 10  219999911111
    E acesso a lista de clientes
    Quando eu removo esse cliente
    Então devo ver a notificação:   Cliente removido com sucesso!
    E esse cliente não deve aparecer na lista