***Settings***
Documentation   login tentativa
Resource    ../../resources/base.robot

Test Template   Tentativa de login

Suite Setup      Start session
Suite Teardown   Finish session
Test Teardown    Finish TestCase

***Keywords***
Tentativa de login
    [Arguments]       ${email}    ${senha}      ${msg_erro}
    Acesso a pagina de login
    Submeto minhas credenciais    ${email}      ${senha}
    Devo ver um alerta com a mensagem de erro   ${msg_erro}

***Test Cases***
Senha inválida      admin@zepalheta.com.br      abc123      Ocorreu um erro ao fazer login, cheque as credenciais.
Senha em branco     admin@zepalheta.com.br      ${EMPTY}    O campo senha é obrigatório!
Email em branco     ${EMPTY}                    pwd123      O campo email é obrigatório!
Email e senha em branco     ${EMPTY}            ${EMPTY}    Os campos email e senha não foram preenchidos!
Email incorreto     admin&gmail.com             abc123      Ocorreu um erro ao fazer login, cheque as credenciais.
